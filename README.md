# From development to production, an introduction on how to test and deploy using CI/CD"
This workshop was created to integrate the [SINFO](https://sinfo.org/) conference (2022 edition).  
The goal of this workshop is to give an overview of how code is usually developed, tested and deployed in enterprises.  
As an example we will develop a CI/CD pipeline to test our simple web application and deploy it on [Heroku cloud platform](https://www.heroku.com/what).   You can follow the presentation here: [SINFO22 - CI/CD Workshop](https://docs.google.com/presentation/d/1hl2teqO5GuCVjQxefBqoVZQ1IRSnIrM03nU7-xhJQXQ/edit?usp=sharing)

# Requirements
- Basic understanding of **Unix**, **Git** and **Python**
- **Gitlab** and **Heroku** accounts
- **Git**, **Python3/Pip/VirtualEnv** installed (see [useful links](#useful-links))

# Workshop Parts

This workshop is splitted into various parts:

1. [Getting started](docs/1.md)
2. [Deploying to Heroku](docs/2.md)
3. [Gitlab CI/CD](docs/3.md)
# Useful links
- [Installing Git {Linux, Windows and MacOS}](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [Installing Python 3 {Linux, Windows and MacOS}](https://wiki.python.org/moin/BeginnersGuide/Download)
- [Installing VirtualEnv](https://gist.github.com/Geoyi/d9fab4f609e9f75941946be45000632b)
