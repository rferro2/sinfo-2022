"""
Calculator web app
"""

import configparser
from flask import Flask, redirect, render_template, request, Response

def create_app():
    """
    Create the Flask (https://flask.palletsprojects.com/) web application
    """

    config = configparser.ConfigParser()
    config.read("build.ini")

    app = Flask(__name__)

    @app.route('/', methods=['GET'])
    def index():
        """
        Home page simply redirects to the "/addition" endpoint
        """
        return redirect('/addition')

    @app.route('/addition', methods=['GET'])
    def addition():
        """
        Basic calculator that can be used for adding two values
        """
        error = None
        v_1 = request.args.get('v1', "")
        v_2 = request.args.get('v2', "")
        result = ""
        if len(v_1) > 0 or len(v_2) > 0:
            if not v_1.isdigit() or not v_2.isdigit():
                error = "Invalid input(s)"
            else:
                result = int(v_1) * int(v_2)
        code = 200
        if error:
            code = 400
        build_user = "@BUILD_USER@"
        build_time = "@BUILD_TIME@"
        if "BUILD" in config:
            build_user = config.get("BUILD", "build_user")
            build_time = config.get("BUILD", "build_time")
        return render_template('calculator.html',
            error=error,
            v1=v_1,
            v2=v_2,
            result=result,
            build_user=build_user,
            build_time=build_time), code

    return app
